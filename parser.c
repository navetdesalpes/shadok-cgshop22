#include "parser.h"


rapidjson::Document readJson(string filename)
{
    ifstream in(filename, ifstream::in | ifstream::binary);
    if (!in.is_open())
    {
        cerr << "Error reading " << filename << endl;
        exit(EXIT_FAILURE);
    }

    rapidjson::IStreamWrapper isw {in};

    rapidjson::Document doc {};
    doc.ParseStream(isw);

    if (doc.HasParseError())
    {
        cerr << "Error  : " << doc.GetParseError()  << endl;
        cerr << "Offset : " << doc.GetErrorOffset() << endl;
        exit(EXIT_FAILURE);
    }
    return doc;
}

void readSolution(graph_t &G,solution_t &sol, rapidjson::Document &doc)
{
    const rapidjson::Value &json_num_colors = doc["num_colors"];
    long num_colors = json_num_colors.GetInt();

    const rapidjson::Value &json_colors = doc["colors"];
    vector<long> colors;
    colors.resize(G.edges.size());
    long index = 0;
    for (auto &ob : json_colors.GetArray()) {
        colors[index] = ob.GetInt();
        index++;
    }

    vector<list<edge_t>> temp_sol;
    temp_sol.resize(num_colors);
    for (index = 0; index < colors.size(); ++index) {
        temp_sol[colors[index]].push_back(G.edges[index]);
    }

    for (long i = 0; i < temp_sol.size(); ++i) {
        subgraph_t subg;
        subg.edges = temp_sol[i];
        sol.graphs.push_back(subg);
    }
}

void read_info_file(rapidjson::Document &doc, list<long> &clique)
{
    const rapidjson::Value& json_easy = doc["clique"];
    for (auto &ob : json_easy.GetArray()) {
        clique.push_back(ob.GetInt());
    }

}

void readInstance(graph_t &G, rapidjson::Document &doc)
{

    const rapidjson::Value &json_id = doc["id"];
    string id = json_id.GetString();
    G.instance = id;

    const rapidjson::Value& json_n = doc["n"];
    int number_nodes = json_n.GetInt();
    G.nodes.resize(number_nodes);

    const rapidjson::Value& json_m = doc["m"];
    int number_edges = json_m.GetInt();
    G.edges.resize(number_edges);

    long index = 0;
    const rapidjson::Value& json_x = doc["x"];
    for (auto &ob : json_x.GetArray()) {
        G.nodes[index].x = ob.GetInt();
        index++;
    }

    index = 0;
    const rapidjson::Value& json_y = doc["y"];
    for (auto &ob : json_y.GetArray()) {
        G.nodes[index].y = ob.GetInt();
        index++;
    }

    index = 0;
    const rapidjson::Value& json_edge_i = doc["edge_i"];
    for (auto &ob : json_edge_i.GetArray()) {
        G.edges[index].node_1 = ob.GetInt();
        index++;
    }

    index = 0;
    const rapidjson::Value& json_edge_j = doc["edge_j"];
    for (auto &ob : json_edge_j.GetArray()) {
        G.edges[index].node_2 = ob.GetInt();
        G.edges[index].id = index;
        index++;
    }
}

int parse(graph_t &G, const char*filename)
{
    string str = std::string(filename);
    rapidjson::Document doc = readJson(str);
    readInstance(G, doc);
    return 0;
}

int parse_info_file(const char *filename, list<long> &clique)
{
    string str = std::string(filename);
    rapidjson::Document doc = readJson(str);
    read_info_file(doc, clique);
    return 0;
}

int load_instance(graph_t &G, const char *instance)
{
    parse(G,instance);
    return 0;
}

int load_solution(graph_t &G, solution_t &sol, const char *instance,
                  const char *sol_file)
{
    parse(G,instance);
    string str = std::string(sol_file);
    rapidjson::Document doc = readJson(str);
    readSolution(G,sol,doc);
    return 0;
}

void readParameters(graph_t &G, rapidjson::Document &doc)
{

    if (doc.HasMember("instance")) {
        const rapidjson::Value &json_instance = doc["instance"];
        string instance = json_instance.GetString();
        G.param.instance_name = (char *)malloc((instance.size()+1)*sizeof(char *));
        strcpy(G.param.instance_name, instance.c_str());
        cout << "instance name is: " << G.param.instance_name << endl;
    } else {
        cout << "Missing mandatory argument from parameters file: instance" << endl;
        exit(-1);
    }

    G.param.solution_name = NULL;
    G.param.info_name = NULL;

    G.param.power = 1.2;
    G.param.noise_mean = 1;
    G.param.noise_var = 0.15;
    G.param.max_queue = -1;
    G.param.max_run_time = 3600;

    G.param.dfs = 1;
    G.param.easy = 1;

    G.param.param_loop = false;
    G.param.param_loop_time = 3600;
    G.param.power_loop[0] = 1.1;
    G.param.power_loop[1] = 1.2;
    G.param.power_loop[2] = 1.3;
    G.param.power_loop[3] = 1.5;
    G.param.power_loop[4] = 2.0;
    G.param.loop_indice = 0;

    if (doc.HasMember("solution")) {
        const rapidjson::Value &json_solution = doc["solution"];
        string solution = json_solution.GetString();
        G.param.solution_name = (char *)malloc((solution.size()+1)*sizeof(char *));
        strcpy(G.param.solution_name, solution.c_str());
        cout << "solution name is: " << G.param.solution_name << endl;
    }

    if (doc.HasMember("info")) {
        const rapidjson::Value &json_info = doc["info"];
        string info = json_info.GetString();
        G.param.info_name = (char *)malloc((info.size()+1)*sizeof(char *));
        strcpy(G.param.info_name, info.c_str());
        cout << "info name is: " << G.param.info_name << endl;
    }

    if (doc.HasMember("power")) {
        const rapidjson::Value &json_power = doc["power"];
        double power = json_power.GetDouble();
        G.param.power = power;
        cout << "power is: " << power << endl;
    }

    if (doc.HasMember("noise_mean")) {
        const rapidjson::Value &json_mean = doc["noise_mean"];
        double mean = json_mean.GetDouble();
        G.param.noise_mean = mean;
        cout << "noise mean is: " << mean << endl;
    }

    if (doc.HasMember("noise_var")) {
        const rapidjson::Value &json_var = doc["noise_var"];
        double var = json_var.GetDouble();
        G.param.noise_var = var;
        cout << "noise var is: " << var << endl;
    }

    if (doc.HasMember("max_queue")) {
        const rapidjson::Value &json_maxQ = doc["max_queue"];
        long maxQ = json_maxQ.GetInt();
        G.param.max_queue = maxQ;
        cout << "max queue is: " << maxQ << endl;
    }

    if (doc.HasMember("max_run_time")) {
        const rapidjson::Value &json_runtime = doc["max_run_time"];
        long runtime = json_runtime.GetInt();
        G.param.max_run_time = runtime;
        cout << "max run time is: " << runtime << endl;
    }

    if (doc.HasMember("dfs")) {
        const rapidjson::Value &json_dfs = doc["dfs"];
        long dfs = json_dfs.GetInt();
        G.param.dfs = dfs;
        cout << "dfs is: " << dfs<< endl;
    }

    if (doc.HasMember("easy")) {
        const rapidjson::Value &json_easy = doc["easy"];
        long easy = json_easy.GetInt();
        G.param.easy = easy;
        cout << "easy is: " << easy << endl;
    }

    if (doc.HasMember("param_loop")) {
        const rapidjson::Value &json_param_loop = doc["param_loop"];
        long loop = json_param_loop.GetInt();
        if (loop == 1) {
            G.param.param_loop = true;
            G.param.power = 1.1;
        }
        cout << "param_loop is: " << loop << endl;
    }

    if (doc.HasMember("param_loop_time")) {
        const rapidjson::Value &json_loop_time = doc["param_loop_time"];
        long loop_time = json_loop_time.GetInt();
        G.param.param_loop_time = loop_time;
        cout << "param_loop_time is: " << loop_time << endl;
    }
}

int load_parameters(graph_t &G, const char *parameters_file)
{
    string str = std::string(parameters_file);
    rapidjson::Document doc = readJson(str);
    readParameters(G, doc);
    return 0;
}
