#ifndef CROMBEZ__H__CGWEEK22__PARSER__H__
#define CROMBEZ__H__CGWEEK22__PARSER__H__

#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>


#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <stack>
#include <unistd.h>
#include <string.h>
#include "rapidjson/document.h"
#include "rapidjson/istreamwrapper.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/ostreamwrapper.h"

#include "solver.h"

using namespace rapidjson;
using namespace std;


int parse_info_file(const char *filename, list<long> &easy);
int parse(graph_t &G, const char*filename);
int load_instance(graph_t &G, const char *instance);
int load_solution(graph_t &G, solution_t &sol, const char *instance,
                  const char *sol_file);

int load_parameters(graph_t &G, const char *parameters_file);

#endif
