CC = g++
CFLAGS = -Ofast -std=c++11
LDFLAGS =
 
SRC = $(wildcard *.c)
OBJS = $(SRC:.c=.o)
 
%.o : %.c
	$(CC) $(CFLAGS) -o $@ -c $<

all: dfs

dfs: dfs_main.o parser.o solver.o geometry.o
	$(CC) $(LDFLAGS) -o dfs dfs_main.o parser.o solver.o geometry.o


clean :
	@rm *.o
cleaner : clean
	@rm $(AOUT)
