#include "parser.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <dirent.h>
#include <string.h>

int test_intersection()
{
    graph_t G;

    G.nodes.resize(100);

    //the node xy is located at (x,y)
    //for instande: 45 is (4,5)
    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < 10; ++j) {
            G.nodes[10*i+j].x = i;
            G.nodes[10*i+j].y = j;
        }
    }

    //the edge abcd is (a,b) to (c,d)
    //for instance 9536 is (9,5) to (3,6)
    G.edges.resize(10000);

    for (int i = 0; i < 100; ++i) {
        for (int j = 0; j < 100; ++j) {
            G.edges[100*i+j].node_1 = i;
            G.edges[100*i+j].node_2 = j;

            G.edges[100*i+j].id = 100*i+j;
        }
    }


    //3 points Vertically aligned, 1 is in the vertical edge, it should
    //intersect
    assert(is_intersecting(G,G.edges[1925], G.edges[2129]) == true);
    assert(is_intersecting(G,G.edges[1925], G.edges[2921]) == true);
    assert(is_intersecting(G,G.edges[2519], G.edges[2921]) == true);
    assert(is_intersecting(G,G.edges[2519], G.edges[2129]) == true);

    assert(is_intersecting(G,G.edges[2129], G.edges[1925]) == true);
    assert(is_intersecting(G,G.edges[2921], G.edges[1925]) == true);
    assert(is_intersecting(G,G.edges[2921], G.edges[2519]) == true);
    assert(is_intersecting(G,G.edges[2129], G.edges[2519]) == true);


    //horizontal aligned touching on one node
    assert(is_intersecting(G,G.edges[2242], G.edges[4252]) == false);
    assert(is_intersecting(G,G.edges[4222], G.edges[4252]) == false);

    //horizontal aligned overlapping
    assert(is_intersecting(G,G.edges[5383],G.edges[9373]) == true);
    assert(is_intersecting(G,G.edges[7393],G.edges[5383]) == true);

    //horizontal aligned one in the other
    assert(is_intersecting(G,G.edges[4494],G.edges[5474]) == true);
    assert(is_intersecting(G,G.edges[7454],G.edges[9444]) == true);



    //vertical aligned touching on one node
    assert(is_intersecting(G,G.edges[2224], G.edges[2425]) == false);
    assert(is_intersecting(G,G.edges[2422], G.edges[2425]) == false);

    //vertical aligned overlapping
    assert(is_intersecting(G,G.edges[3538],G.edges[3739]) == true);
    assert(is_intersecting(G,G.edges[3739],G.edges[3538]) == true);

    //vertical aligned one in the other
    assert(is_intersecting(G,G.edges[4449],G.edges[4547]) == true);
    assert(is_intersecting(G,G.edges[4745],G.edges[4944]) == true);

    //oblique aligned touching on one node
    assert(is_intersecting(G,G.edges[1345],G.edges[7745]) == false);
    assert(is_intersecting(G,G.edges[7745],G.edges[4513]) == false);

    //oblique aligned overlapping
    assert(is_intersecting(G,G.edges[1135],G.edges[2347]) == true);
    assert(is_intersecting(G,G.edges[3511],G.edges[2347]) == true);

    //oblique aligned one in the other
    assert(is_intersecting(G,G.edges[1147],G.edges[2335]) == true);
    assert(is_intersecting(G,G.edges[4711],G.edges[2335]) == true);

    // standard crossing
    assert(is_intersecting(G,G.edges[1344],G.edges[1541]) == true);
    assert(is_intersecting(G,G.edges[2356],G.edges[6149]) == true);

    //Shared node with turn
    assert(is_intersecting(G,G.edges[2356],G.edges[5638]) == false);
    assert(is_intersecting(G,G.edges[3856],G.edges[5623]) == false);

    //Intersect on one non shared node
    assert(is_intersecting(G,G.edges[4452],G.edges[2266]) == true);
    assert(is_intersecting(G,G.edges[6622],G.edges[5244]) == true);

    //Full miss
    assert(is_intersecting(G,G.edges[1133],G.edges[5473]) == false);
    assert(is_intersecting(G,G.edges[5473],G.edges[3311]) == false);


    //one line intersects the segment, but not the other wway around
    assert(is_intersecting(G,G.edges[2346],G.edges[3766]) == false);
    assert(is_intersecting(G,G.edges[6637],G.edges[2346]) == false);


    //share a node, coaligned, overlap
    assert(is_intersecting(G,G.edges[1143],G.edges[1175]) == true);
    assert(is_intersecting(G,G.edges[7511],G.edges[1143]) == true);

    //share a node, coaligned, no overlap
    assert(is_intersecting(G,G.edges[1143],G.edges[4375]) == false);
    assert(is_intersecting(G,G.edges[7543],G.edges[1143]) == false);

    return 0;
}

int main(int argc, char **argv)
{
#if 0
    if (argc < 3) {
        cout << "requires 2 arguments: instance solution" << endl;
        cout <<"3 rd optional argument: info file name" << endl;
        return 1;
    } else {
        instance_name = argv[1];
        solution_name = argv[2];
        if (argc > 3) {
            info_name = argv[3];
        }
    }
#endif
    if (argc < 2) {
        cout << "Requires 1 argument: the json files of parameters" << endl;
        cout << "Mandatory parameters in the json file:" << endl;
        cout << "instance: Path to the instance file" << endl << endl;

        cout << "Optional parameters in the json file:" << endl;
        cout << "solution: Path to the solution file you want to optimize. Default value: empty: create a new solution" << endl << endl;
        cout << "info: Path to the info file containing the largest known clique. Default value: empty: No clique used" << endl;
        cout << "power: Penalty power used to penalize queud edges. Default value: 1.2" << endl;
        cout << "noise_mean: Mean of the gaussian noise. Default value: 1" << endl;
        cout << "noise_var: Variance of the gaussian noise. Default value: 0.15" << endl;
        cout << "max_queue: Maximum number of times an edge can be queued. Default value: 200*(75000/n)^2" << endl;
        cout << "max_run_time: Maximum running time in seconds before stopping. Default: 3600" << endl;

        cout << "dfs: Activate dfs: 1, or do not use dfs: 0. Default: 1" << endl;
        cout << "easy: Activate easy segments: 1, or do not use easy segments: 0. Default: 1" << endl;
        cout << "param_loop: Activates parameters change every param_loop_time seconds. Activates: 1, do not use: anything else. Default value: 0" << endl;
        cout << "param_loop_time: time in seconds between each parameters change for param_loop. Default value: 3600" << endl;
    }

#if 0
    test_intersection();
#endif

    graph_t G;

    load_parameters(G, argv[1]);

    solution_t sol;


    clock_t start = clock();
    G.param.start_time = start;

    if (G.param.solution_name) {
        load_solution(G, sol, G.param.instance_name, G.param.solution_name);
    } else {
        load_instance(G, G.param.instance_name);
        init_solution(G, sol);
    }

    list<pair<double,int>> dp;
    sol.data_points = dp;


    if (G.param.max_queue < 1) {
        long card = G.edges.size();
        double nb_repetition = 75000.0 / card;
        nb_repetition = nb_repetition * nb_repetition;
        nb_repetition = nb_repetition * 2000;

        G.param.max_queue = nb_repetition;
    }


    list<long> clique;
    if (G.param.info_name) {
        parse_info_file(G.param.info_name, clique);
    }
    if (clique.size() == sol.graphs.size()) {
        cout << "File is optimal" << endl;
        return 0;
    }

    while (true) {
        dfs_optimize(G,sol,clique,(long) G.param.max_queue, G.param.easy == 1);

        clock_t end;
        end = clock();
        double time_spent = (double) (end - start) / CLOCKS_PER_SEC;
        if (time_spent > G.param.max_run_time) {
            break;
        }
    }

    free(G.param.instance_name);
    free(G.param.solution_name);
    free(G.param.info_name);

    return 0;
}

