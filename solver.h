#ifndef __CROMBEZ_SHOP22_SOLVER_H__
#define __CROMBEZ_SHOP22_SOLVER_H__

#include "geometry.h"
#include <list>
#include <utility>

typedef struct subgraph_t {
    list<edge_t> edges;
} subgraph_t;

typedef struct stack_event_t {
    bool was_it_removed;
    bool was_it_added;
    list<subgraph_t>::iterator was_removed_from;
    list<subgraph_t>::iterator was_added_to;
    edge_t edge;
} stack_event_t;



typedef struct solution_t {
    list<subgraph_t> graphs;

    list<pair<double,int>> data_points;
} solution_t;

int dfs_optimize(graph_t &G, solution_t &sol,
                 list<long> &clique, int MAX_QUEUE_TIMES,
                 bool is_using_easy);


int conflict_dfs_optim_solution(graph_t &G, solution_t &sol,
                               list<long> &clique,
                               vector<vector<unsigned int>> &intersections,
                               int MAX_QUEUE_TIMES, bool one_shot, bool is_using_easy);


int init_solution(graph_t &G, solution_t &sol);

#endif
