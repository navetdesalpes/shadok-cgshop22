set style line 1 lc rgb '#191970' pt 6 ps 1 lt 1 lw 2 # midnightblue
set style line 2 lc rgb '#006400' pt 8 ps 1 lt 1 lw 2 # darkgreen
set style line 3 lc rgb '#ff0000' pt 2 ps 1 lt 1 lw 2 # ref
set style line 4 lc rgb '#ffd700' pt 4 ps 1 lt 1 lw 2 # gold
set style line 5 lc rgb '#00ff00' pt 1 ps 1 lt 1 lw 2 # lime
set style line 6 lc rgb '#00ffff' pt 6 ps 1 lt 1 lw 2 # aqua
set style line 7 lc rgb '#ff00ff' pt 8 ps 1 lt 1 lw 2 # fuchsia
set style line 8 lc rgb '#ffb6c1' pt 2 ps 1 lt 1 lw 2 # lightpink


set style line 11 lc rgb '#808080' lt 1
set border 3 back ls 11
set tics nomirror
set style line 12 lc rgb '#808080' lt 0 lw 1
set grid back ls 12
set key horiz
set key top right Left reverse
set key box linestyle 11
set xtics textcolor rgb '#000000'
set ytics textcolor rgb '#000000'

set yrange [218:250]
set xrange [0:1]
set xtics 1
set ytics 10
set xlabel 'Running time (cpu hours)'
set ylabel 'Number of colors'

set terminal svg size 1000,500 fname 'serif'  rounded dashed
set output 'variance_no_dfs_1h.svg'
set title 'Number of colors over time (no dfs)'
plot \
"vispecn13806_info-1_power-1.200000_noise_mean-1.000000_noise_var-0.000000_max_queue-1500000_max_run_time-432000_dfs-0_easy-1_loop-0:3600" using ($1/3600):2 title 'V:0.00' with steps ls 1, \
"vispecn13806_info-1_power-1.200000_noise_mean-1.000000_noise_var-0.050000_max_queue-1500000_max_run_time-432000_dfs-0_easy-1_loop-0:3600" using ($1/3600):2 title 'V:0.05' with steps ls 6, \
"vispecn13806_info-1_power-1.200000_noise_mean-1.000000_noise_var-0.100000_max_queue-1500000_max_run_time-432000_dfs-0_easy-1_loop-0:3600" using ($1/3600):2 title 'V:0.10' with steps ls 3, \
"vispecn13806_info-1_power-1.200000_noise_mean-1.000000_noise_var-0.150000_max_queue-1500000_max_run_time-432000_dfs-0_easy-1_loop-0:3600" using ($1/3600):2 title 'V:0.15' with steps ls 8, \
"vispecn13806_info-1_power-1.200000_noise_mean-1.000000_noise_var-0.200000_max_queue-1500000_max_run_time-432000_dfs-0_easy-1_loop-0:3600" using ($1/3600):2 title 'V:0.20' with steps ls 5, \
"vispecn13806_info-1_power-1.200000_noise_mean-1.000000_noise_var-0.250000_max_queue-1500000_max_run_time-432000_dfs-0_easy-1_loop-0:3600" using ($1/3600):2 title 'V:0.25' with steps ls 2, \
"vispecn13806_info-1_power-1.200000_noise_mean-1.000000_noise_var-0.300000_max_queue-1500000_max_run_time-432000_dfs-0_easy-1_loop-0:3600" using ($1/3600):2 title 'V:0.30' with steps ls 7, \
"vispecn13806_info-1_power-1.200000_noise_mean-1.000000_noise_var-0.350000_max_queue-1500000_max_run_time-432000_dfs-0_easy-1_loop-0:3600" using ($1/3600):2 title 'V:0.35' with steps ls 4, \




