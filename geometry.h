#ifndef CROMBEZ__H__CGWEEK22__GEOM__H__
#define CROMBEZ__H__CGWEEK22__GEOM__H__

using namespace std;
#include <vector>
#include <string>

#define MAX(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })

#define MIN(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })


typedef struct node_t {
    long x;
    long y;
} point_t;

typedef struct edge_t {
    long node_1;
    long node_2;
    long id;
} edge_t;

typedef struct parameters_t {
    char *instance_name;
    char *solution_name;
    char *info_name;

    double power;
    double noise_mean;
    double noise_var;
    long max_queue;
    long max_run_time;

    long dfs;
    long easy;

    clock_t start_time;

    bool param_loop;
    int param_loop_time;
    double power_loop[5];
    long loop_indice;


} parameters_t;

typedef struct graph_t {
    vector<node_t> nodes;
    vector<edge_t> edges;
    string instance;

    vector<vector<unsigned int>> intersections;

    parameters_t param;
} graph_t;



long get_twice_triangle_signed_area(node_t &p1, node_t &p2, node_t &p3);
bool are_co_aligned(node_t &p1, node_t &p2, node_t &p3);
bool is_clockwise(node_t &p1, node_t &p2, node_t &p3);
bool is_counter_clockwise(node_t &p1, node_t &p2, node_t &p3);


bool is_strictly_clockwise(node_t &p1, node_t &p2, node_t &p3);
bool is_strictly_counter_clockwise(node_t &p1, node_t &p2, node_t &p3);

bool is_intersecting(graph_t &G, edge_t &e1, edge_t &e2);
bool is_intersecting(vector<vector<unsigned int>> &intersections,
                     edge_t &e1, edge_t &e2);

int generate_intersection_map(graph_t &G,
                              vector<vector<unsigned int>> &intersections);

#endif
