#include "solver.h"
#include <iostream>
#include <fstream>
#include <time.h>
#include <random>
#include <limits.h>
#include <algorithm>

clock_t BEGIN_TIME;




std::default_random_engine generator;
std::normal_distribution<double> *distribution;



int add_data_point_to_graph_file(graph_t &G,solution_t &sol)
{
    clock_t end = clock();
    double time_spent = (double) (end - BEGIN_TIME) / CLOCKS_PER_SEC;

    pair<double,int> data_point(time_spent,sol.graphs.size());
    sol.data_points.push_back(data_point);

    string info = "0";
    if (G.param.info_name) {
        info = "1";
    }

    string fn;
    if (G.param.param_loop == 0) {
        if (G.param.solution_name == NULL) {
            fn = "./graphs/" + G.instance +
                "_info-" + info +
                "_power-" + to_string(G.param.power) +
                "_noise_mean-" + to_string(G.param.noise_mean) +
                "_noise_var-" + to_string(G.param.noise_var) +
                "_max_queue-" + to_string(G.param.max_queue) +
                "_max_run_time-" + to_string(G.param.max_run_time) +
                "_dfs-" + to_string(G.param.dfs) +
                "_easy-" + to_string(G.param.easy) +
                "_loop-" + to_string(G.param.param_loop) + ':' + to_string(G.param.param_loop_time);
        } else {
            fn = "./graphs/" + G.instance +
                "_solution-" + (&G.param.solution_name[24]) +
                "_info-" + info +
                "_power-" + to_string(G.param.power) +
                "_noise_mean-" + to_string(G.param.noise_mean) +
                "_noise_var-" + to_string(G.param.noise_var) +
                "_max_queue-" + to_string(G.param.max_queue) +
                "_max_run_time-" + to_string(G.param.max_run_time) +
                "_dfs-" + to_string(G.param.dfs) +
                "_easy-" + to_string(G.param.easy) +
                "_loop-" + to_string(G.param.param_loop) + ':' + to_string(G.param.param_loop_time);
        }
    } else {
        if (G.param.solution_name == NULL) {
            fn = "./graphs/" + G.instance +
                "_info-" + info +
                "_power-" + "loop" +
                "_noise_mean-" + to_string(G.param.noise_mean) +
                "_noise_var-" + to_string(G.param.noise_var) +
                "_max_queue-" + to_string(G.param.max_queue) +
                "_max_run_time-" + to_string(G.param.max_run_time) +
                "_dfs-" + to_string(G.param.dfs) +
                "_easy-" + to_string(G.param.easy) +
                "_loop-" + to_string(G.param.param_loop) + ':' + to_string(G.param.param_loop_time);
        } else {
            fn = "./graphs/" + G.instance +
                "_solution-" + (&G.param.solution_name[24]) +
                "_info-" + info +
                "_power-" + "loop" +
                "_noise_mean-" + to_string(G.param.noise_mean) +
                "_noise_var-" + to_string(G.param.noise_var) +
                "_max_queue-" + to_string(G.param.max_queue) +
                "_max_run_time-" + to_string(G.param.max_run_time) +
                "_dfs-" + to_string(G.param.dfs) +
                "_easy-" + to_string(G.param.easy) +
                "_loop-" + to_string(G.param.param_loop) + ':' + to_string(G.param.param_loop_time);
        }
    }

    ofstream file(fn);

    for (pair<double,int> &pr: sol.data_points) {
        file << pr.first << " " << pr.second << endl;
    }

    file.close();
    return 0;
}

/*
 * Write the solution sol, for the instance G, in the file named file_name
 * G: in: the instance
 * sol: in: the solution
 * file_name: in: Name of the file
 * */
int write_solution(graph_t &G, solution_t &sol, string file_name)
{


    ofstream file(file_name);

    file << "{" << endl << "\"type\": \"Solution_CGSHOP2022\"," << endl;
    file << "\"instance\": \"" << G.instance << "\"," << endl;
    file << "\"num_colors\": " << sol.graphs.size() << "," << endl;
    file << "\"colors\": ";

    vector<long> colors;
    colors.resize(G.edges.size());

    list<subgraph_t>::iterator graph_it;
    graph_it = sol.graphs.begin();
    int color = 0;



    while (graph_it != sol.graphs.end()) {
        list<edge_t>::iterator edge_it;
        edge_it = graph_it->edges.begin();
        while (edge_it != graph_it->edges.end()) {
            //cout << edge_it->id << endl;
            colors[edge_it->id] = color;
            ++edge_it;
        }
        ++color;
        ++graph_it;
    }



    file << "[";
    file << colors[0];
    for (int i = 1; i < colors.size(); ++i) {
        file << ", " << colors[i];
    }
    file << "]" << endl << "}";
    file.close();

    return 0;
}


/*
 * Compare the size of two subgraphs.
 * This function is used to sort colors by their sizes
 * */
bool compare_size_graph(const subgraph_t &g1, const subgraph_t &g2)
{
    return g1.edges.size() < g2.edges.size();
}

/*
 * Return true if the edge e can be added to the color subg
 * G: in: instance
 * e: in: the edge
 * subg: in: the color
 * intersctions: in: intersection graph of all the edges
 * */
bool edge_can_be_added_to_graph(graph_t &G, edge_t &e, subgraph_t &subg,
                                vector<vector<unsigned int>> &intersections)
{
    list<edge_t>::iterator it;
    it = subg.edges.begin();
    while (it != subg.edges.end()) {
        // XXX
        if (is_intersecting(intersections, e, *it)) {
            return false;
        }
        ++it;
    }
    return true;
}


/*
 * Try to optimize a solution by moving the edges from color to color. As
 * a result, the solution is shuffled even when no optimization is done
 * G: in: instance
 * sol: in/out: the colors
 * interseciont: in: intersection graph of all edges
 * */
int optimize_solution(graph_t &G, solution_t &sol,
                      vector<vector<unsigned int>> &intersections)
{
    //order subgraphs from smallest to largest
    sol.graphs.sort(compare_size_graph);

    //for each subgraph, for each of its edges, we try to move the edge to
    //another graph
    list<subgraph_t>::iterator it;
    it = sol.graphs.begin();
    while (it != sol.graphs.end()) {
        list<edge_t>::iterator edge_it;
        edge_it = it->edges.begin();

        while (edge_it != it->edges.end()) {
            //try to move the edge to another subgraph
            bool is_moved = false;
            list<subgraph_t>::iterator other_sub_it;
            other_sub_it = sol.graphs.begin();
            while (other_sub_it != sol.graphs.end()) {
                if (other_sub_it == it) {
                    ++other_sub_it;
                    continue;
                }
                if (edge_can_be_added_to_graph(G,*edge_it, *other_sub_it,
                                               intersections))
                {
                    //we move the edge to the other subgraph
                    edge_t the_edge;
                    the_edge.node_1 = edge_it->node_1;
                    the_edge.node_2 = edge_it->node_2;
                    the_edge.id = edge_it->id;

                    edge_it = it->edges.erase(edge_it);
                    other_sub_it->edges.push_back(the_edge);
                    is_moved = true;
                    break;
                }
                ++other_sub_it;
            }
            if (is_moved) {
                continue;
            } else {
                ++edge_it;
            }
        }

        if (it->edges.size() == 0) {
            it = sol.graphs.erase(it);
        } else {
            it++;
        }
    }
    return 0;
}

/* copy a solution: s1 = s2
 * s1: out where the solution must be copied
 * s2: in solution to copy
 */
int copy_sol(solution_t &s1, solution_t &s2)
{
    list<subgraph_t>::iterator graph_it;
    graph_it = s2.graphs.begin();
    while (graph_it != s2.graphs.end()) {
        subgraph_t copy_graph;

        list<edge_t>::iterator edge_it;
        edge_it = graph_it->edges.begin();
        while (edge_it != graph_it->edges.end()) {
            edge_t copy;
            copy.node_1 = edge_it->node_1;
            copy.node_2 = edge_it->node_2;
            copy.id = edge_it->id;

            copy_graph.edges.push_back(copy);

            ++edge_it;
        }
        s1.graphs.push_back(copy_graph);
        ++graph_it;
    }
    return 0;
}



/*
 * Find the color with the smallest score. This is the color in which the edge
 * will be added by the conflict optimizer.
 * Return false if all subgraphs have an intersection with an already been
 * queued edge
 * return true otherwise
 *
 * G: in: instance
 * sol: in: colors
 * edge_to_place: in: the edge that we want to move to a color
 * has_been_in_queue: in: The number of times that each edge has already been
 * in the queue. (Used to compute the score)
 * graph_of_list_conflict: out: The chosen color the put the edge_to_place
 * conflicting_edges: out: The list of edges in the chosen color that
 * intersect edge_to_place
 * intersections: in: Graph of edges intersections
 * MAX_QUEUE_TIMES: in: Hard limit on the number of times and edge can be
 * queued
 * */
bool find_graph_of_least_conflict(graph_t &G, solution_t &sol,
                                  edge_t &edge_to_place, vector<int> &has_been_in_queue,
                                  list<subgraph_t>::iterator &graph_of_least_conflict,
                                  list<list<edge_t>::iterator> &conflicting_edges,
                                  vector<vector<unsigned int>> &intersections,
                                  int MAX_QUEUE_TIMES)
{
    double min_conflict =  MAX_QUEUE_TIMES * G.edges.size();

    list<subgraph_t>::iterator graph_it;
    graph_it = sol.graphs.begin();

    while (graph_it != sol.graphs.end()) {
        list<edge_t>::iterator edge_it;
        double conflict_count = 0;
        list<list<edge_t>::iterator> candidate_edges;

        //XXX some gaussian noise
        double noise = (*distribution)(generator);

        //don t want stupidly low noise, or even worse: negative noise
        while (noise < 0.001) {
            noise = (*distribution)(generator);
        }
        double min_conflict_noised = min_conflict / noise;

        edge_it = graph_it->edges.begin();
        while (edge_it != graph_it->edges.end()) {
            //XXX
            if (is_intersecting(intersections, edge_to_place, *edge_it)) {
                if (has_been_in_queue[edge_it->id] >= MAX_QUEUE_TIMES) {
                    //It s no good
                    conflict_count = min_conflict_noised + 1;
                    break;
                } else {
                    //conflict_count = conflict_count + has_been_in_queue[edge_it->id] * 0.97 + 1;
                    conflict_count = conflict_count + pow(has_been_in_queue[edge_it->id], G.param.power) + 1;
                    if (conflict_count >= min_conflict_noised) {
                        break;
                    }
                    candidate_edges.push_back(edge_it);
                }
            }
            ++edge_it;
        }

        if (conflict_count * noise < min_conflict) {
            min_conflict = conflict_count * noise;
            graph_of_least_conflict = graph_it;
            conflicting_edges.clear();

            list<list<edge_t>::iterator>::iterator the_it;
            the_it = candidate_edges.begin();
            while (the_it != candidate_edges.end()) {
                conflicting_edges.push_back(*the_it);
                ++the_it;
            }
        }

        graph_it++;
    }

    if (min_conflict < MAX_QUEUE_TIMES * G.edges.size()) {
        return true;
    }

    return false;
}


/*
 * Return true fi edge is in easy. False otherwise
 * edge: the edge
 * easy: list of easy edges
 * */
bool is_an_easy_edge(long edge, list<long> &easy)
{
    list<long>::iterator it;
    for (it = easy.begin(); it != easy.end(); ++it) {
        if (*it == edge) {
            return true;
        }
    }
    return false;
}



/*
 * Rebuild a full solution, from the partial solution lacking the easy edges
 * G: in instance
 * sol: in/out The colors (without the easy edges when in) the full solution
 * (including the easy edge when out)
 * easy: in: list of easy edges
 * */
int rebuild_full_sol(graph_t &G, solution_t &sol, list<long> &easy)
{

    //Test whether or not we use easy
    if (G.param.easy != 1) {
        return 0;
    }

    list<long>::reverse_iterator r_it;

    if (G.intersections.size() == 0) {
        generate_intersection_map(G, G.intersections);
    }

    cout << "There are " << easy.size() << " edges to greedy color" << endl;

    for (r_it = easy.rbegin(); r_it != easy.rend(); ++r_it) {
        //greedy coloring
        list<subgraph_t>::iterator it;

        bool colored = false;
        for (it = sol.graphs.begin(); it != sol.graphs.end(); ++it) {
            if (edge_can_be_added_to_graph(G, G.edges[*r_it],
                                           *it, G.intersections))
            {
                colored = true;
                //add it
                it->edges.push_back(G.edges[*r_it]);
                break;
            }
        }
        if (!colored) {
            cout << "ERR: could not greedy color the graph" << endl;
            return -1;
        }
    }
    //save file
    //string name = "./optimized/" + G.instance + "_sol_from_easy_edges" + "_size_" + to_string(sol.graphs.size());
    //write_solution(G, sol, name);

    return 0;
}

/*
 * Compute the list of easy edges for an instance and an objective color
 * number
 * G: in: the instance
 * intersections: in: intersection map of the edges
 * easy_segment: out: list of easy segments
 * color_number: the number of color we aim for
 * */
int compute_easy_segments(graph_t &G,
                          vector<vector<unsigned int>> &intersections,
                          list<long> &easy_segment,
                          int color_number)
{
    vector<long> degree;
    degree.resize(G.edges.size());

    for (int i = 0; i < G.edges.size(); ++i) {
        degree[i] = 0;
        for (int j = 0; j < G.edges.size(); ++j) {
            if (i == j) {
                continue;
            }
            if (is_intersecting(G, G.edges[i], G.edges[j])) {
                degree[i]++;
            }
        }
    }

    while (true) {
        int s = 0;
        //Get the min degree
        for (int i = 1; i < degree.size(); ++i) {
            if (degree[i] < degree[s]) {
                s = i;
            }
        }
        if (degree[s] > color_number - 1) {
            break;
        }

        degree[s] = INT_MAX;
        easy_segment.push_back(s);
        for (int i = 0; i < degree.size(); ++i) {
            if (degree[i] != INT_MAX &&
                is_intersecting(G, G.edges[i], G.edges[s]))
            {
                degree[i]--;
            }
        }
    }
    return 0;
}

/* Inclusion test: is e in forbidden ?*/
bool is_in(edge_t &e, list<edge_t> &forbidden)
{
    for (edge_t &forbid: forbidden) {
        if (forbid.id == e.id) {
            return true;
        }
    }
    return false;
}


/*
 * Compute the list of edge intersecting e in color.
 * Return -1 if more there is more than max_intersections intersections
 * Return -1 if at least 1 forbidden edge is intersected by e in color
 * Return 0 otherwise
 * G: in: instance
 * color: in: the color
 * e: in: the edge
 * intersectors: out: the list of edges from color that intersect e
 * max_intersections: in: maximum number of allowed intersections before
 * stopping computation and returning -1
 * forbidden: in: List of forbidden edges that we are not allowed to intersect
 * */
int get_intersectors(graph_t &G, subgraph_t &color, edge_t &e,
                     list<edge_t> &intersectors, int max_intersections,
                     list<edge_t> &forbidden)
{
    int count = 0;
    list<edge_t>::iterator edge_it;
    for (edge_it = color.edges.begin(); edge_it != color.edges.end();
         ++edge_it)
    {
        if (is_intersecting(G.intersections, e, *edge_it)) {
            count++;
            if (count > max_intersections) {
                return -1;
            }
            if (is_in(*edge_it, forbidden)) {
                return -1;
            }
            /*
               cout << "The forbidden list: " << endl;
               for (edge_t &e: forbidden) {
               cout << e.id << endl;
               }
               cout << "NOT FORBIDDEN: " << e.id << endl;
               */
            intersectors.push_back(*edge_it);
        }
    }
    return 0;
}


/*
 * return all the colors that have a number of conflict less or equal than
 * breadth
 * the colors are returned in conflict_colors, that stores, the color, and the
 * list of edges that intersects
 * G: in: instance
 * sol: in: the colors
 * e: in: the edge
 * breadth: in: max number of conflicts
 * forbidden: list of forbidden edges, we can't intersect those
 * conflict_colors: out: list of the colors that intersects e less than
 * breadth time
 * */
int get_colors_with_small_conflict_number(graph_t &G, solution_t &sol,
                                          edge_t &e, int breadth,
                                          list<edge_t> &forbidden,
                                          list<pair<list<subgraph_t>::iterator, list<edge_t>>> &conflict_colors)
{

    list<subgraph_t>::iterator it_colors;
    for (it_colors = sol.graphs.begin(); it_colors != sol.graphs.end();
         ++it_colors)
    {
        list<edge_t> intersectors;
        if (get_intersectors(G, *it_colors, e, intersectors, breadth, forbidden) != -1) {

            pair<list<subgraph_t>::iterator, list<edge_t>> intersect(it_colors, intersectors);

            conflict_colors.push_back(intersect);
        }
    }
    return 0;
}


/* Comp operator for sort*/
bool cmp_conflict_color(
    pair<list<subgraph_t>::iterator, list<edge_t>> &p1,
    pair<list<subgraph_t>::iterator, list<edge_t>> &p2)
{
    return get<1>(p1).size() < get<1>(p2).size();
}

/* Sort according to the number of conflicting edges*/
int order_conflict_colors(
    list<pair<list<subgraph_t>::iterator, list<edge_t>>> &conflict_colors)
{
    conflict_colors.sort(cmp_conflict_color);
    return 0;
}

/*Remove the edge from the color
 * only removes the first occurence
 * color: in/out: color from which to erase e
 * e: in: edge to remove
 */

int remove_edge(list<edge_t> &color, edge_t &e)
{
    list<edge_t>::iterator it;
    for (it = color.begin(); it != color.end(); ++it) {
        if (e.id == it->id) {
            color.erase(it);
            return 0;
        }
    }
    return 1;
}

/*
 * Add conflicts to color
 * color: in/out: the color to which we add the edges
 * conflicts: in: edges to add to color
 * */
int add_edges_to_color(list<edge_t> &color, list<edge_t> &conflicts)
{
    for (edge_t &e: conflicts) {
        color.push_back(e);
        //cout << "Pushing: " << e.id << endl;
    }
    return 0;
}

/*
 * remove e from color
 * color: in/out: color from which to remov e
 * e: in: edge to remove from color
 * */
int remove_edge_from_color(list<edge_t> &color, edge_t &e)
{

    list<edge_t>::iterator it_color = color.begin();
    while (true) {
        if (it_color->id == e.id) {
            color.erase(it_color);
            return 0;
        }
        ++it_color;
        if (it_color == color.end()) {
            return -1;
        }
    }
}

/*
 * remove all the edges in conflicts from color
 * color: in/out: color from chich we remove edges
 * conflicts: in: list of edges to remove from color
 * */
int remove_edges_from_color(list<edge_t> &color, list<edge_t> &conflicts)
{
    bool found = false;

    if (conflicts.size() == 0) {
        return 0;
    }

    list<edge_t>::iterator it_color = color.begin();
    list<edge_t>::iterator it_conf = conflicts.begin();
    while (true) {
        if (it_color->id == it_conf->id) {
            ++it_conf;
            //cout << "Removing:  " << it_color->id << endl;
            it_color = color.erase(it_color);
            found = true;
            if (it_conf == conflicts.end()) {
                return 0;
            }
        } else {
            it_color++;
            if (it_color == color.end()) {
                if (!found) {
                    exit(-1);
                } else {
                    //Start over
                    found = false;
                    it_color = color.begin();
                }
            }
        }
    }
    return -1;
}

/*
 * Undo a stacked event that removed an edge from a color
 * */
int undo_removed(stack_event_t &evt)
{
    //we have to add it back
    evt.was_removed_from->edges.push_back(evt.edge);
    return 0;
}

/*
 * undo a stacked event that added an edge to a color
 * */
int undo_added(stack_event_t &evt)
{
    //We have to remove it
    list<edge_t>::iterator e_it;
    for (e_it = evt.was_added_to->edges.begin();
         e_it != evt.was_added_to->edges.end();
         e_it++)
    {
        if (e_it->id == evt.edge.id) {
            evt.was_added_to->edges.erase(e_it);
            return 0;
        }
    }
    return -1;
}

/*
 * undo a single stacked event
 * */
int undo_change(stack_event_t &evt)
{
    if (evt.was_it_added) {
        //cout << "was added" << endl;
        undo_added(evt);
    } else {
        //cout << "was removed" << endl;
        undo_removed(evt);
    }
    return 0;
}

/*
 * undo all stacked events and pop them out of the stack
 * */
int undo_changes(list<stack_event_t> &changes)
{
    while (changes.size() != 0) {
        undo_change(changes.back());
        changes.pop_back();
    }
    return 0;
}

/*
 * Tries a limited Depth First Search in the tree of possibility in order to
 * add an edge (or several during recursive calls)
 * to a color without having to queue any other edges
 * if we failed, sol remains unchanged, otherwise the colors are changed
 * G: in: instance
 * sol: in/out: colors
 * todo: in: list of edges that need to find a color
 * forbidden: in: list of edges that we can't intersect and move to the recursive
 * todo list
 * breadth: Maximum number of intersection within a color allowed
 * depth: Maximum depth in the tree of possibility to reach
 * changes: Stack of all the changes that have been made to reach the current
 * state of sol
 * */
int dfsOptimize(graph_t &G, solution_t &sol, list<edge_t> &todo,
                list<edge_t> &forbidden, int breadth, int depth,
                list<stack_event_t> &changes)
{
    list<edge_t> copy_forbidden = forbidden;

    if (todo.size() == 0) {
        return 0;
    }
    if (depth == 1) {
        breadth = 0;
    }

    //All edges in todo must be colored
    for (edge_t &e : todo) {
        list<pair<list<subgraph_t>::iterator, list<edge_t>>> conflict_colors;

        get_colors_with_small_conflict_number(G, sol, e, breadth, forbidden,
                                              conflict_colors);
        bool has_been_breaked = false;
        for (pair<list<subgraph_t>::iterator, list<edge_t>> &conflicts_c:
             conflict_colors)
        {
            get<0>(conflicts_c)->edges.push_front(e);

            remove_edges_from_color(get<0>(conflicts_c)->edges,
                                    get<1>(conflicts_c));
            list<edge_t> subtodo = get<1>(conflicts_c);


            list<stack_event_t> status;
            copy_forbidden.push_back(e);
            int ret = dfsOptimize(G,sol, subtodo, copy_forbidden,
                                  breadth, depth - 1, status);
            copy_forbidden.pop_back();

            if (ret == -1) {
                list<edge_t> edge_to_remove;
                edge_to_remove.push_back(e);

                remove_edges_from_color(get<0>(conflicts_c)->edges,
                                        edge_to_remove);

                add_edges_to_color(get<0>(conflicts_c)->edges,
                                   get<1>(conflicts_c));

            } else {
                stack_event_t change;
                change.was_it_removed = false;
                change.was_it_added = true;
                change.was_added_to = get<0>(conflicts_c);
                change.edge = e;
                changes.push_back(change);

                for (edge_t &cf: get<1>(conflicts_c)) {
                    stack_event_t change;
                    change.was_it_removed = true;
                    change.was_it_added = false;
                    change.was_removed_from = get<0>(conflicts_c);
                    change.edge = cf;
                    changes.push_back(change);
                }
                for (stack_event_t &st: status) {
                    changes.push_back(st);
                }
                has_been_breaked = true;
                break;
            }
        }
        // No possibility, abort
        if (!has_been_breaked) {
            undo_changes(changes);
            return -1;
        }
    }
    return 0;
}


/*
 * Try to optimize sol using the conflict optimizer and the dfs technique
 * handles the shuffling and multistart aspect of the algorithm
 * G: in: instance
 * sol: in/out: the colors
 * clique: in: largest known clique
 * MAX_QUEUE_TIMES: in: maximum number of times an edge is allowed to be queued
 * is_using_easy: in: true: if we are using easy edges, false otherwise
 * */
int dfs_optimize(graph_t &G, solution_t &sol,
                 list<long> &clique, int MAX_QUEUE_TIMES,
                 bool is_using_easy)
{

    BEGIN_TIME = G.param.start_time;
    string name;


    distribution = new std::normal_distribution<double>(G.param.noise_mean,
                                                        G.param.noise_var);

    if (G.intersections.size() == 0) {
        generate_intersection_map(G, G.intersections);
    }

    list<long> easy;
    if (is_using_easy) {
        int obj_number = sol.graphs.size() - 1;
        compute_easy_segments(G, G.intersections, easy, obj_number);
        cout << "easy size: " << easy.size() << endl;

        //remove the easy segments
        //On supprime les easy edge de la solution
        list<subgraph_t>::iterator it;
        for (it = sol.graphs.begin();
             it != sol.graphs.end();
             ++it)
        {
            list<edge_t>::iterator edge_it;
            for (edge_it = it->edges.begin();
                 edge_it != it->edges.end();
            )
            {
                if (is_an_easy_edge(edge_it->id, easy)) {
                    //we remove the edge
                    edge_it = it->edges.erase(edge_it);
                } else {
                    ++edge_it;
                }
            }
        }
    }

    long sol_cpt = 0;
    while (true) {
        long old_size = 0;
        long size = sol.graphs.size();
        int cpt = 0;

        //Some shuffling
        //XXX REMOVED SHUFFLING FOR TEST
        while (cpt < 11) {
            cout << "size of solution: " << size << "   " << cpt << endl;

            optimize_solution(G,sol,G.intersections);
            old_size = size;
            size = sol.graphs.size();

            if (old_size == size) {
                cpt++;
            } else {
                cpt = 0;
                /*
                name = "./optimized/" + G.instance + "_sol_" + to_string(sol_cpt++) + "_size_" + to_string(sol.graphs.size());
                if (is_using_easy) {
                    name = "./optimized/" + G.instance + "_sol_partial_from_easy_coloring" + to_string(sol_cpt++) + "_size_" + to_string(sol.graphs.size());
                }
                write_solution(G, sol, name);
                */
                if (is_using_easy) {
                    rebuild_full_sol(G,sol,easy);
                    goto write_full_sol;
                }
            }
        }

        old_size = sol.graphs.size();
        long new_size = 0;
        while (new_size != old_size) {

            old_size = sol.graphs.size();
            conflict_dfs_optim_solution(G, sol, clique, G.intersections,
                                        MAX_QUEUE_TIMES, false, is_using_easy);

            new_size = sol.graphs.size();

            cout << "sol size is: " << new_size << endl;
            if (old_size != new_size) {
                rebuild_full_sol(G,sol,easy);
                goto write_full_sol;
            }

            //If We are running for more than MAX_RUN_TIME: we are out
            clock_t end = clock();
            double time_spent = (double) (end - BEGIN_TIME) / CLOCKS_PER_SEC;
            if (time_spent > G.param.max_run_time) {
                free(distribution);
                return 0;
            }

        }
    }

  write_full_sol:
    clock_t end = clock();
    double time_spent = (double) (end - BEGIN_TIME) / CLOCKS_PER_SEC;

    //name = "./optimized/" + G.instance + "_FOR_PAPER_EASY_"+ to_string(G.param.easy) +"_TIME_SPENT_" + to_string(time_spent) + "_size_" + to_string(sol.graphs.size());
    cout << "Writing solution of size" << sol.graphs.size() << endl;
    write_solution(G,sol, name);

    name = "./optimized/" + G.instance + ".best.sol.json";
    write_solution(G,sol, name);

    //Also write the new data point for the paper graph
    add_data_point_to_graph_file(G,sol);

    free(distribution);
    return 0;
}



/*
 * Try to optimize sol using the conflict optimizer and the dfs technique
 *
 * G: in: instance
 * sol: in/out: the colors
 * clique: in: largest known clique
 * instersections: in: map of edge intersections
 * MAX_QUEUE_TIMES: in: maximum number of times an edge is allowed to be queued
 * one_shot: in: artefact TODO remove
 * is_using_easy:in: true: if we are using easy edges, false otherwise
 */
int conflict_dfs_optim_solution(graph_t &G, solution_t &sol,
                                list<long> &clique,
                                vector<vector<unsigned int>> &intersections,
                                int MAX_QUEUE_TIMES, bool one_shot, bool is_using_easy)
{
    int DEBUG_COUNT = 0;
    //
    //order subgraphs from smallest to largest
    sol.graphs.sort(compare_size_graph);

    //for each subgraph, for each of its edges, we try to move the edge to
    //another graph
    list<subgraph_t>::iterator it;
    it = sol.graphs.begin();

    while (it != sol.graphs.end()) {
        list<edge_t>::iterator edge_it;
        edge_it = it->edges.begin();

        while (edge_it != it->edges.end()) {
            //try to move the edge to another subgraph
            bool is_moved = false;
            list<subgraph_t>::iterator other_sub_it;
            other_sub_it = sol.graphs.begin();
            while (other_sub_it != sol.graphs.end()) {
                if (other_sub_it == it) {
                    ++other_sub_it;
                    continue;
                }
                if (edge_can_be_added_to_graph(G,*edge_it, *other_sub_it,
                                               intersections))
                {
                    //we move the edge to the other subgraph
                    edge_t the_edge;
                    the_edge.node_1 = edge_it->node_1;
                    the_edge.node_2 = edge_it->node_2;
                    the_edge.id = edge_it->id;

                    edge_it = it->edges.erase(edge_it);
                    other_sub_it->edges.push_back(the_edge);
                    is_moved = true;
                    break;
                }
                ++other_sub_it;
            }
            if (is_moved) {
                continue;
            } else {
                ++edge_it;
            }
        }

        if (it->edges.size() == 0) {
            it = sol.graphs.erase(it);
        } else {


            cout << "entering conflict solver for the " << ++DEBUG_COUNT << " time" << endl;
            //We didn't manage to move every edge. We start the conflict solver
            list<edge_t> queue;
            solution_t temp_sol;


            //This is a save. In case we do not succeed to improve, we'll
            //restore the save
            copy_sol(temp_sol, sol); //temp_sol = sol

            //We move the remaining edges to the queue, and delete the
            //subgraph
            list<edge_t>::iterator local_edge_it;
            local_edge_it = it->edges.begin();
            while (local_edge_it != it->edges.end()) {
                queue.push_back(*local_edge_it);
                local_edge_it++;
            }

            long first_edge_id_of_it = it->edges.begin()->id;

            it = sol.graphs.erase(it);


            //Now, for each edge in the queue, we move it to the subgraph with
            //least conflict, and move the conflicting edges to the queue
            //Repeat until the queue is empty, or until we can't find a graph
            //such that we would re-queue an edge (put into the queue an edge
            //that already went into the queue this generation)

            vector<int> has_been_in_queue(G.edges.size(),0);
            bool successfull_removal = true;
            //XXX put clique edge weight to infty
            list<long>::iterator clique_it;
            for (clique_it = clique.begin();
                 clique_it != clique.end();
                 ++clique_it)
            {
                has_been_in_queue[*clique_it] = INT_MAX;
            }

            list<edge_t> dfs_queue;
            while (queue.size() != 0 || dfs_queue.size() != 0) {

                clock_t end = clock();
                double time_spent = (double) (end - BEGIN_TIME) / CLOCKS_PER_SEC;
                //test for stopping running
                if (time_spent > G.param.max_run_time) {
                    free(distribution);
                    exit(0);
                }
                //Test for switching params
                if (G.param.param_loop && time_spent > G.param.param_loop_time *
                    (G.param.loop_indice + 1))
                {
                    cout << "Switching param" << endl;
                    cout << G.param.power_loop[0] << endl;
                    cout << G.param.power_loop[1] << endl;
                    cout << G.param.power_loop[2] << endl;
                    cout << G.param.power_loop[3] << endl;
                    cout << G.param.power_loop[4] << endl;
                    G.param.loop_indice++;
                    G.param.power = G.param.power_loop[G.param.loop_indice % 5];
                    cout << "New power is: " << G.param.power << endl;
                }


                edge_t edge_to_place;
                if (dfs_queue.size() != 0) {
                    edge_to_place.node_1 = dfs_queue.front().node_1;
                    edge_to_place.node_2 = dfs_queue.front().node_2;
                    edge_to_place.id = dfs_queue.front().id;

                    list<edge_t> solo;

                    solo.push_back(edge_to_place);
                    list<stack_event_t> stack;

                    //We try to put it, if we succed good, else it goes to the
                    //conflict solver

                    list<edge_t> forbidden;
                    int depth = 3;
                    if (queue.size() < 3) {
                        if (queue.size() == 1) {
                            depth = 5;
                        } else {
                            depth = 7;
                        }
                    }
                    if (G.param.dfs == 1 &&
                        dfsOptimize(G,sol, solo,forbidden, 3,depth,stack) == 0) {
                        //we succeeded
                        dfs_queue.pop_front();
                        continue;
                    } else {
                        queue.push_back(dfs_queue.front());
                        dfs_queue.pop_front();
                        continue;
                    }
                } else {
                    edge_to_place.node_1 = queue.front().node_1;
                    edge_to_place.node_2 = queue.front().node_2;
                    edge_to_place.id = queue.front().id;
                    queue.pop_front();
                }


                //find the graph of least conflict
                list<subgraph_t>::iterator graph_of_least_conflict;
                list<list<edge_t>::iterator> conflicting_edges;

                if (find_graph_of_least_conflict(G, sol, edge_to_place,
                                                 has_been_in_queue,
                                                 graph_of_least_conflict,
                                                 conflicting_edges,
                                                 intersections,
                                                 MAX_QUEUE_TIMES))
                {
                    //move the conflicted edges to the queue
                    list<list<edge_t>::iterator>::iterator conf_edges_it;
                    conf_edges_it = conflicting_edges.begin();
                    while (conf_edges_it != conflicting_edges.end()) {
                        edge_t add_to_queue;

                        add_to_queue.node_1 = (*conf_edges_it)->node_1;
                        add_to_queue.node_2 = (*conf_edges_it)->node_2;
                        add_to_queue.id = (*conf_edges_it)->id;

                        //add conflict edges to queue
                        dfs_queue.push_back(add_to_queue);

                        //remove conflict edges from graph
                        graph_of_least_conflict->edges.erase(*conf_edges_it);

                        ++conf_edges_it;
                    }
                    //add edge_to_place to the graph of least conflict
                    graph_of_least_conflict->edges.push_back(edge_to_place);

                    //flag edge_to_place as untouchable from now on
                    has_been_in_queue[edge_to_place.id] = has_been_in_queue[edge_to_place.id] + 1;
                } else {
                    //all graphs have conflicts with an old queued edge
                    //Stop computation, and restore to temp_sol

                    cout << "MAX RUN TIME REACHED" << endl;

                    sol.graphs.clear();
                    copy_sol(sol, temp_sol); // sol = temp_sol

                    //restore it
                    list<subgraph_t>::iterator restore_it;

                    restore_it = sol.graphs.begin();
                    while (true) {
                        if (restore_it->edges.begin()->id == first_edge_id_of_it) {
                            it = restore_it;
                            break;
                        } else {
                            ++restore_it;
                        }
                    }


                    successfull_removal = false;
                    break;
                }

            }
            if (successfull_removal == false) {
                ++it;
                if (one_shot) {
                    return 0;
                } else {
                    cout << "No one shot" << endl;
                }
            } else {
                cout << "REMOVED a color" << endl;
                return 0;
            }
            //XXX Stio uf running for more than MAX_RUN_TIME
            clock_t end = clock();
            double time_spent = (double) (end - BEGIN_TIME) / CLOCKS_PER_SEC;
            if (time_spent > G.param.max_run_time) {
                return 0;
            }


        }
    }
    return 0;
}


double get_angle(graph_t &G, edge_t &e) {
    long dx, dy;
    dx = G.nodes[e.node_2].x - G.nodes[e.node_1].x;
    dy = G.nodes[e.node_2].y - G.nodes[e.node_1].y;

    if (dx < 0) {
        dx = -dx;
        dy = -dy;
    }
    return atan2(dy,dx);
}


bool compare_angle(pair<int,double> &a1, pair<int,double> &a2)
{
    return a1.second < a2.second;
}

/*
 * Creates a simple initial solution by greedily adding edges to colors
 * G: in: instance
 * sol: out: solution
 * */
int init_solution(graph_t &G, solution_t &sol)
{
    generate_intersection_map(G, G.intersections);

    /*
       vector<pair<int,double>> angles;
       int ind = 0;
       for (edge_t &e: G.edges) {
       double angle = get_angle(G, e);
       pair<int,double> pr = make_pair(ind, angle);
       ind++;
       angles.push_back(pr);
       }

       sort(angles.begin(), angles.end(), compare_angle);
       for (pair<int,double> &pr: angles) {
       edge_t e = G.edges[pr.first];
       bool is_inserted = false;
       for (subgraph_t &subg: sol.graphs) {
       if (edge_can_be_added_to_graph(G, e,subg, G.intersections)) {
       subg.edges.push_back(e);
       is_inserted = true;
       break;
       }
       }
       if (!is_inserted) {
    //Can t be added to any color.
    //We must create a new color
    subgraph_t new_color;
    new_color.edges.push_back(e);
    sol.graphs.push_back(new_color);
    }
    }
    */


    for (edge_t &e: G.edges) {
        bool is_inserted = false;
        for (subgraph_t &subg: sol.graphs) {
            if (edge_can_be_added_to_graph(G, e,subg, G.intersections)) {
                subg.edges.push_back(e);
                is_inserted = true;
                break;
            }
        }
        if (!is_inserted) {
            //Can t be added to any color.
            ////We must create a new color
            subgraph_t new_color;
            new_color.edges.push_back(e);
            sol.graphs.push_back(new_color);
        }
    }

    cout << "We have this many colors: " << sol.graphs.size() << endl;
    return 0;
}

