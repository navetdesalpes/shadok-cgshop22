#include "geometry.h"

#include <iostream>
using namespace std;


long get_twice_triangle_signed_area(node_t &p1, node_t &p2, node_t &p3)
{
    long ux,uy,vx,vy;
    ux = p2.x - p1.x;
    vx = p3.x - p1.x;

    uy = p2.y - p1.y;
    vy = p3.y - p1.y;

    return ux*vy - uy*vx;
}


bool are_co_aligned(node_t &p1, node_t &p2, node_t &p3)
{
    return get_twice_triangle_signed_area(p1,p2,p3) == 0;
}

bool is_clockwise(node_t &p1, node_t &p2, node_t &p3)
{
    return get_twice_triangle_signed_area(p1,p2,p3) <= 0;
}

bool is_counter_clockwise(node_t &p1, node_t &p2, node_t &p3)
{
    return get_twice_triangle_signed_area(p1,p2,p3) >= 0;
}

bool is_strictly_clockwise(node_t &p1, node_t &p2, node_t &p3)
{
    return get_twice_triangle_signed_area(p1,p2,p3) < 0;
}

bool is_strictly_counter_clockwise(node_t &p1, node_t &p2, node_t &p3)
{
    return get_twice_triangle_signed_area(p1,p2,p3) > 0;
}

/* Test the intersection of two edges
 * A common node is not considered and intersection
 * G: in: the graph
 * e1: in: the first edge in G
 * e2: in: the second edge in G
 * */
bool is_intersecting(graph_t &G, edge_t &e1, edge_t &e2)
{
    if (e1.node_1 == e2.node_1 || e1.node_1 == e2.node_2 ||
        e1.node_2 == e2.node_1 || e1.node_2 == e2.node_2)
    {
        //There is one common node
        node_t e11,e12,e21,e22;
        e11.x = G.nodes[e1.node_1].x;
        e11.y = G.nodes[e1.node_1].y;

        e12.x = G.nodes[e1.node_2].x;
        e12.y = G.nodes[e1.node_2].y;

        e21.x = G.nodes[e2.node_1].x;
        e21.y = G.nodes[e2.node_1].y;

        e22.x = G.nodes[e2.node_2].x;
        e22.y = G.nodes[e2.node_2].y;

        long a1, a2;

        a1 = get_twice_triangle_signed_area(e11,e12,e21);
        a2 = get_twice_triangle_signed_area(e11,e12,e22);
        if (a1 != 0 || a2 != 0) {
            //There is an angle, hence they don t overlap
            return false;
        }
        //The 3 nodes are co-aligned
        //there is no overlap if the bbox are not overlapping

        long min1,max1,min2,max2;
        if (e11.x != e12.x) {
            //not vertical we use x values
            min1 = MIN(e11.x, e12.x);
            max1 = MAX(e11.x, e12.x);

            min2 = MIN(e21.x, e22.x);
            max2 = MAX(e21.x, e22.x);
        } else {
            //vertical alignment we use y values
            min1 = MIN(e11.y, e12.y);
            max1 = MAX(e11.y, e12.y);

            min2 = MIN(e21.y, e22.y);
            max2 = MAX(e21.y, e22.y);
        }

        if (min1 == max2 || min2 == max1) {
            return false;
        } else {
            return true;
        }

    }
    // The 4 nodes are different

    node_t e11,e12,e21,e22;
    e11 = G.nodes[e1.node_1];
    e12 = G.nodes[e1.node_2];

    e21 = G.nodes[e2.node_1];
    e22 = G.nodes[e2.node_2];

    long a1, a2;

    a1 = get_twice_triangle_signed_area(e11,e12,e21);
    a2 = get_twice_triangle_signed_area(e11,e12,e22);
    if ((a1 > 0 && a2 > 0) || (a1 < 0 && a2 < 0)) {
        return false;
    }

    if (a1 == 0 && a2 == 0) {
        //all co aligned
        long min1,max1,min2,max2;
        if (e11.x != e12.x) {
            //not vertical we use x values
            min1 = MIN(e11.x, e12.x);
            max1 = MAX(e11.x, e12.x);

            min2 = MIN(e21.x, e22.x);
            max2 = MAX(e21.x, e22.x);
        } else {
            //vertical alignment we use y values
            min1 = MIN(e11.y, e12.y);
            max1 = MAX(e11.y, e12.y);

            min2 = MIN(e21.y, e22.y);
            max2 = MAX(e21.y, e22.y);
        }

        if (min1 > max2 || min2 > max1) {
            return false;
        } else {
            return true;
        }
    }

    a1 = get_twice_triangle_signed_area(e21,e22,e11);
    a2 = get_twice_triangle_signed_area(e21,e22,e12);
    if ((a1 > 0 && a2 > 0) || (a1 < 0 && a2 < 0)) {
        return false;
    }


    return true;
}



int generate_intersection_map(graph_t &G,
                              vector<vector<unsigned int>> &intersections)
{
    int size = ((G.edges.size()-1) / 32) + 1;
    intersections.resize(G.edges.size());
    for (int i = 0; i < G.edges.size(); ++i) {
        intersections[i].resize(size);
    }

    for (int i = 0; i < G.edges.size(); ++i) {
        int index_i;
        int shift_i;

        index_i = i / 32;
        shift_i = i % 32;

        for (int j = i + 1; j < G.edges.size(); ++j) {

            if (is_intersecting(G, G.edges[i], G.edges[j])) {
                int index_j;
                int shift_j;

                index_j = j / 32;
                shift_j = j % 32;

                intersections[i][index_j] =
                    intersections[i][index_j] | (1 << shift_j);
                intersections[j][index_i] =
                    intersections[j][index_i] | (1 << shift_i);
            }
        }
    }
    return 0;
}

bool is_intersecting(vector<vector<unsigned int>> &intersections,
                     edge_t &e1, edge_t &e2)
{
    int i = e1.id;
    int j = e2.id;

    int index_j, shift_j;
    index_j = j / 32;
    shift_j = j % 32;

    return (intersections[i][index_j] & (1 << shift_j));
}
